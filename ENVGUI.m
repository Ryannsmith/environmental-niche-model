% ENVGUI.m
% Jacob Anderson
% 12-01-2017

% This generates a GUI for doing environmental niche modeling.

close all
clear all
clc

global MainWindow;                          % Main GUI window
global EnvWindow;                           % Environmental Niche Modeling GUI window
global Model;                               % Area model being developed by this app
global DATA;                                % Sensor Data
global UserInputs;                          % Variable to save and access saved user data  
global shape
Model  = struct('WaterMap',{},'MapParameter',{},'ParamiterSettings',[1,0],'WaterMapIO',false,'ParameterStats',[]);

UserInputs = SavedUserInputs(mfilename);    % Create an file for saving data or acces previously saved data
Start()                                     % Start the GUI


function Start(~)
global MainWindow;   % Main GUI window

windowName = 'Akabotics GUI';
panalName = 'Main';

MainWindow = GUIwindow(windowName);                                         % Instanciate the GUI
MainWindow.GUIpanels(panalName,'make','panel',[0.8 0 0.2 1]);  % Create putton panel for primary GUI functions

MainWindow.Header.String = 'Select Data Input';                             % Prompt to select data


Panel = MainWindow.Panels(panalName);                                       % Extract the button pane from its mapped container to access the structure
Panel.panel.BackgroundColor = [0.6 0.9 1];                                  % * Mapped containers do not allow multileve indexing

% Make buttons in the button panel --> {Buttin type, Button Name, Callback function and their inputs, Callback function for slider listeners
MainWindow.GUIbuttons( panalName,'add', ...
    {'pushbutton','Load Geotiff',          [{@GetGeoTiff},      {MainWindow},{panalName}],[];   %  1
    'pushbutton', 'Color Threshold',       [{@ColorThreshold},  {MainWindow},{panalName}],[];   %  2
    'space',      ' ',                     [],                                            [];   %  3
    'pushbutton', 'Load Shape File',       [{@GetShapeFile},    {MainWindow},{panalName}],[];   %  4
    'pushbutton', 'Clear Shape File',      [{@ClearShapeFile},  {MainWindow},{panalName}],[];   %  5
    'space',      ' ',                     [],                                            [];   %  6
    'pushbutton', 'Make Occupancy Grid',   [{@MakeOcupancyGrid},{MainWindow},{panalName}],[];   %  7
    'space',      ' ',                     [],                                            [];   %  8
    'pushbutton', 'Load Data',             [{@LoadData},        {MainWindow},{panalName}],[];   %  9
    'pushbutton', 'Clear Data',            [{@ClearData},       {MainWindow},{panalName}],[];   % 10
    'pushbutton', 'Make Env. Niche Model', [{@MakeEnvNiche},    {MainWindow},{panalName}],[];   % 11
    'space',      ' ',                     [],                                            [];   % 12
    'pushbutton', 'Clear All',             [{@ClearAll},        {MainWindow},{panalName}],[];   % 13
%    'pushbutton', 'Save',                  [{@SaveAll},         {MainWindow},{panalName}],[];   % 14
    });

Panel = MainWindow.Panels(panalName);   % Put the new version of the panel into its mapped container

% Create tool strips that explain the buttons' function
Panel.button( 1).Push.TooltipString = sprintf('Select a goe-referances image\nFile extensions: .tif, .tiff');                       % Load Geotiff
Panel.button( 2).Push.TooltipString = sprintf('Color threshold Geotiff\nColor threshold is used to seperate land from water\nUse "Make Occupancy Grid function to apply the color thresholding');      % Colorthreshold
Panel.button( 4).Push.TooltipString = sprintf('Select a list of waypoints that define the area of interest\nFile extensions: .txt');    % Load Shape File
Panel.button( 7).Push.TooltipString = sprintf('Refine area of interest');    % Make Occ. Grid
Panel.button( 9).Push.TooltipString = sprintf('Load Sensor Data\nChose a directory containing the data files\n All files in that directory will be read in\nFile extensions: .log');    % Load Data
Panel.button(11).Push.TooltipString = sprintf('Mkae Environmental Niche Model\nChoose parameters for model\nSet threasholds for creating the model');    % Make Env. Niche Model

% Hide buttons that are not ready of use
Panel.button(2).Push.Visible  = 'off';  % Color Threashold
Panel.button(5).Push.Visible  = 'off';  % Clear shape file
Panel.button(7).Push.Visible  = 'off';  % Make Occ Grid 
Panel.button(10).Push.Visible = 'off';  % Clear Data 
Panel.button(11).Push.Visible = 'off';  % Make Env. Model 

end


function GetGeoTiff(varargin)
% Varargin: {'UIControl', 'matlab.ui.eventdata.ActionData', 'GUIwindow', 'panalName'}

global MainWindow;
global UserInputs;
global geoImage;
global geoData;
global LON;         % Longitude grid for geo-referencing
global LAT;         % Latatude  grid for geo-referanceing


% User Interface to select geotiff file
disp('Select Geotiff');
MainWindow.Header.String = 'Select Geotiff file: .tif, .tiff';                             % Prompt to select data
try 
    [geotiff_name, geotiff_filepath] = uigetfile('*.tif*','Select Geo-tiff',UserInputs.Preferences('GeoTiff')); % Attempt to use a previously saved filepath
catch
    [geotiff_name, geotiff_filepath] = uigetfile('*.tif*','Select Geo-tiff');   % Use default file path if a saved filepath is unavailable
end

if geotiff_name == 0                                                        % Check if the Data Input box was cnaceled
    disp('Input box Cancelled')
    return                                                                  % End script if there isn't an input for it to use
else
    geotiff_fullfilepath = fullfile(geotiff_filepath, geotiff_name);        % generate a full file path that is appropriate for the OS
    [geoImage, geoData] = geotiffread(geotiff_fullfilepath);                % Read goe-tiff file
    
    geoImage = im2uint8(geoImage);                                          % Convert raster image to uint8 data type for faster graphic rendering
    
    if ~isa(geoData,'map.rasterref.GeographicCellsReference')               % Check that geoData is in the right format for 'geoshow'
        
        geoData = georasterref('RasterSize',size(geoImage), ...             % Reformat geoData for 'geoshow'
            'RasterInterpretation', geoData.RasterInterpretation, ...
            'LongitudeLimits',geoData.XWorldLimits, ...
            'LatitudeLimits',geoData.YWorldLimits, ...
            'ColumnsStartFrom',geoData.ColumnsStartFrom,...
            'RowsStartFrom',geoData.RowsStartFrom);
    end
    
end

UserInputs.Preferences('GeoTiff') = geotiff_fullfilepath;                   % Save goetiff filepath for later use
UserInputs.SavePreferences()


% Create geo-referanced mesh for use throughout the program
[m, n, ~] = size(geoImage);
x = linspace(geoData.LongitudeLimits(1),geoData.LongitudeLimits(2),n);
y = linspace(geoData.LatitudeLimits(2), geoData.LatitudeLimits(1), m);
[LON, LAT] = meshgrid(x,y);

MainWindow.BackGround('LON',LON,'LAT',LAT,'GeoData',geoData,'GeoImage',geoImage);

% Display Geotiff
if any(ismember(keys(varargin{3}.Layers),'OcupancyGrid'))
    MakeOcupancyGrid(varargin{:});
        
end

MainWindow.Header.String = 'Select Data Input or do Color threshold';                             % Prompt to select data

% Make Button for Color thresholding visible
Panel = MainWindow.Panels(varargin{4});
Panel.button(2).Push.Visible = 'on';
end


function GetShapeFile(varargin)
% Varargin: {'UIControl', 'matlab.ui.eventdata.ActionData', 'GUIwindow', 'panalName'}

global MainWindow;
global UserInputs;
global shape;

% User Interface to select shape file
MainWindow.Header.String = 'Select Shape File if Available (.txt) or Cancel Dialog Box';                             % Prompt to select data
disp('Select Shape File');
try
    [shape_name, shape_filepath] = uigetfile('*.txt*','Select Shape File',UserInputs.Preferences('Shape'));
catch
    [shape_name, shape_filepath] = uigetfile('*.txt*','Select Shape File');
end

if shape_name == 0                                                          % Check if the Data Input box was cnaceled
    disp('Input box Cancelled')
    return                                                                  % End script if there isn't an input for it to use
else
    shape = ImportWayPoints(shape_filepath,shape_name,'\n');                % Import shape file from .txt file
    
    if shape(1) > 0
        shape = fliplr(shape);                                              % Make sure the waypoints are ordered [Lon Lat]
    end                                                                     % * This only works in the north west hemispher

    if shape(1,2) ~= shape(end)                                             % Check if th eshape file is closed
        shape = [shape;shape(1,1),shape(1,2)];
    end
        
    MainWindow.Addlayer('Shape',shape,varargin{4});                        % Dispaly shape on the GUI
end


UserInputs.Preferences('Shape') = fullfile(shape_filepath, shape_name);     % Save file path for later use
UserInputs.SavePreferences()

Panel = MainWindow.Panels(varargin{4});    % Make buttons usable
Panel.button(5).Push.Visible = 'on';             % Clear shape file
Panel.button(7).Push.Visible = 'on';             % Make Occupany Grid
Panel.button(5).Push.Enable = 'on';              % Enable "clear shape file" button, this will be disabled if the shape file is cleared
end


function ClearShapeFile(varargin)
% Varargin: {'UIControl', 'matlab.ui.eventdata.ActionData', 'GUIwindow', 'panalName'}

global MainWindow;
global shape;
shape = [];                                 % Make shape and empty array
MainWindow.DeleteLayer('Shape')            % Remove layer and its buttons from the GUI
Panel = MainWindow.Panels(varargin{4});    % Make buttons usable
Panel.button(5).Push.Enable  = 'off';       % Turn off the "Clear shape file" button
end


function ColorThreshold(varargin)
% Varargin: {'UIControl', 'matlab.ui.eventdata.ActionData', 'GUIwindow', 'panalName'}
global MainWindow;
% global geoImage;

% colorThresholder(geoImage);                                              % Launcth Matlabe's built in color thresholding app
ColorThresholder;


MainWindow.Header.String = 'Select Data Input or Make Occupancy Grid';  % Prompt to select data

Panel = MainWindow.Panels( varargin{4});                                % Make the 'Make Cccupany Grid' Button visible 
Panel.button(7).Push.Visible = 'on';
end


function LoadData(varargin)
% Varargin: {'UIControl', 'matlab.ui.eventdata.ActionData', 'GUIwindow', 'panalName'}

global MainWindow;
global UserInputs;
global shape;
global DATA

% User Interface to get data files --------------------------------------------------------------------------------------------------------------
disp('Select Data folder');
MainWindow.Header.String = 'Select Folder Containing log files';                             % Prompt to select data
try
    filePath = uigetdir(UserInputs.Preferences('DataFolder'),'Select Data Folder');
catch
    filePath = uigetdir('*.tif*','Select Geo-tiff');
end

if filePath == 0                                        % Check if the Data Input box was cnaceled
    disp('Input box Cancelled')
    return                                              % End script if there isn't an input for it to use
else
    UserInputs.Preferences('DataFolder') = filePath;    % Add file path to saved inputs
end


% Run Function to import Ecomapper data
set(MainWindow.Figure, 'pointer', 'watch')
drawnow;
    
DATA = ReadData(filePath);

set(MainWindow.Figure, 'pointer', 'arrow')
drawnow;

dataPoints = cat(1,DATA.vehicle);                                               % Extrace data waypointds from the data structure for visulizations

MainWindow.Addlayer('DataPoints',dataPoints,varargin{4});                       % Add datapoints to the GUI

MainWindow.Header.String = 'Make Ocupancy Grid or Environmantal Niche Model';   % Prompt to select data

Panel = MainWindow.Panels( varargin{4});
Panel.button(10).Push.Visible = 'on';                                           % Make the "Clear Data" button visible
Panel.button(11).Push.Visible = 'on';                                           % Make the 'Make Env. Niche Model' button visible

UserInputs.SavePreferences()                                                    % Save new filepathes

end


function ClearAll(varargin)
% Varargin: {'UIControl', 'matlab.ui.eventdata.ActionData', 'GUIwindow', 'panalName'}
disp('Clear all')
run('AkaboticsGUI.m')
end


function SaveAll(varargin)
global MainWindow;                          % Main GUI window
global EnvWindow;                           % Environmental Niche Modeling GUI window
global Model;                               % Area model being developed by this app
global DATA;                                % Sensor Data
global UserInputs;


% MainWindow.GUIbuttons( varargin{4},'remove', {'Load Geotiff'});

% UserInputs.SavePreferences()
% uisave({'MainWindow','EnvWindow','Model','DATA'},[num2str(yyyymmdd(datetime('today'))),'_EnvironmentalNicheModel']);


disp('Save all')
end

