# Environmental Niche Model

A basic GUI for creating an Environmental Niche Model from a geotiff and existing environmental data.

The videos located in ~/Videos provide an overview of the GUI and it’s functionality. The PDF files in ~/FlowCharts provide an block-diagram overview of the structure of the code and the GUI functionality.

To use this GUI modify the ReadData.m script (in ~/src) to read in the data that you are using and assign it to the DATA structure used in the GUI.

To integrate your environmental niche model, go into the MakeEnvNiche script and insert your model into the MakeEnvNiche.m function on Line 238.