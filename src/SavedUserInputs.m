% Jacob Anderson
% December 12th 2017

% This is a classe deffinition that will allow useres to save UI inputs and then call the inputs at a later time
% Inputs are saved to Documents/MATLAB/Script_Inputs/<Script_Name>
%   <Script_Name> is the name of the script calling this classdef
%___________________________________________________________________________________________________________________________________________
% To use this script place the following commands in your script:
% 1)    UserInputs = SavedUserInputs(mfilename);            -->  Instanciate the SavedUserInputs Class.
%                                                                * Run this command once at the begining of your script.
%                                                                * This create a file for saving data or acces previously saved data.
%                                                                * mfilename is a MATLAB command that identifies the script being run.
%                                                                  Do not change this command unless you know what you're doing.

% 2)    UserInputs.Preferences('Varialbe_ID') = variable;   -->  Add a varialbe to this instance of the class.
%                                                                * Run this command, followed by the next one, everytime you want to save a variable. 
%                                                                * 'Variable ID' is an arbatrary string that is used to identify the variable.
%                                                                * variable is the input to be saved. Usualy a file path or name but can be anything.

% 3)    UserInputs.SavePreferences();                       -->  This command saves the class instance to the hard drive. The varialbes can then be accessed at a later time

% 4)    variable = UserInputs.Preferences('Variable_ID');   -->  Retrive the saved data
%__________________________________________________________________________________________________________________________________________

classdef SavedUserInputs
    properties
        FilePath
        FullFileName
        Preferences
        
    end
    
    methods
% Initialize the SaveUserInput Class. Check to see if inputs have already been saved ---------------------------------------------------------------------------
        function obj = SavedUserInputs(scriptName)
            
            obj.FilePath     = fullfile( userpath,'Script_Inputs');
            obj.FullFileName = fullfile( userpath,'Script_Inputs',[scriptName,'_suggestedFilePaths.mat']);
            obj.Preferences = containers.Map('KeyType', 'char' ,'ValueType','any');
            
            if exist(obj.FullFileName,'file')                              % If the variable exists, Load the saved settings
                disp('Loading Saved Inputs')
                load(obj.FullFileName);
                obj.Preferences = suggestions;
                
            elseif ~exist(obj.FilePath,'dir')                              % Create the directory for the saved inputs if it does not exist
                disp('Making directory for saved inputs')
                mkdir(obj.FilePath)
                
            else                                                           % The directoy exists, but preferences have not been saved before
                disp('No Saved Inputs')
                return
            end
            
        end
        
        
% Save User Inputs -------------------------------------------------------------------------------------------------------------------------------------------------
        function SavePreferences(obj,varargin)
            suggestions = obj.Preferences;
            save(obj.FullFileName,'suggestions');
            
        end
        
    end
end
