
%% ReadData  ==========================================================================================================================================================
function Data = ReadData(filePath)

% Check that the file path is valid
if ~exist(filePath,'dir')
    error(' --- Invaled file path to data ---')
else
    fprintf('\n Importing EcoMapper logs... \n\n')
end


%% Create Data Structure

Data = struct('vehicle',[],'date',[],'bathy',[],'wqData',[],'Header','');
Data(1).Header = 'vehicle: Latitude [deg.dd], Longitude [deg.dd], depth from surface [m]';
Data(2).Header = 'Date and time: MM.dd.yyyy HH:mm:ss.SS';
Data(3).Header = 'bathy: Latitude [deg.dd], Longitude [deg.dd], depth [m]';

% This line is used by the GUI to set up button names. Change this text to reflect the data that is being used.
%   * Make sure the order of the parameter names matches the order in which the data is entered into the data structure
Data(4).Header = 'wqData: Temp [c], SpCond [mS/cm], Sal [ppt], pH, Turbid + NTU, Chl [ug/L], BGA-PC [cells/ml], ODO [%], ODO [mg/L]';

Data(5).Header = 'Header: Explaned the entries in this data structure';


%% Read in your data here
%   Read in data
%   Seperate paramiters
%   Apply correction, if any


%% Assign data to "DATA" structure ---------------------------------------------------------------------------------------------------------------------------------------------------------
for index = 1:length(Latitude)
    
    % Vehicles position
    Data(index).vehicle(1) = Latitude(index);     % Vehicle's latitude
    Data(index).vehicle(2) = Longitude(index);    % Vehicle's longitude
    Data(index).vehicle(3) = DFSDepthm(index);    % Vehicle's depth
    
    % Date and Time
    Data(index).date = timeStamp(index);          % Date and time of the measurment
    
    % bathymetry data
    Data(index).bathy(1) = Lat_(index);           % Calculated latitude of depth reading 
    Data(index).bathy(2) = Lon_(index);           % Calculated longitude of depth reading
    Data(index).bathy(3) = depth_(index,3);          % Attitude corrected depth reading
    
    % Water peramiter data
    Data(index).wqData(1) = TempC(index);         % Temp [c]
    Data(index).wqData(2) = SpCondmScm(index);    % SpCond [mS/cm]
    Data(index).wqData(3) = Salppt(index);        % Sal [ppt]
    Data(index).wqData(4) = pH(index);            % pH
    Data(index).wqData(5) = TurbidNTU(index);     % Turbid + NTU
    Data(index).wqData(6) = ChlugL(index);        % Chl [ug/L]
    Data(index).wqData(7) = BGAPCcellsmL(index);  % BGA-PC [cells/ml]
    Data(index).wqData(8) = ODOsat(index);        % ODO [%]
    Data(index).wqData(9) = ODOmgL(index);        % ODO [mg/L]
    
end


end


