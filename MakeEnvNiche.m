function MakeEnvNiche(varargin)
global DATA;            % Sensor data
global EnvWindow;       % Environmental Niche Modeling GUI window
global shape;           % List of waypoints provided by the user. Waypoints outline the area of interest
global Boundaries;      % The different areas found when evaluating the OccupancyGrid
global xGrid;           % Longitude mesh for the modeling area
global yGrid;           % Latitude mesh for the modeling area
global geoImage;        % Geotiff image
global geoData;         % Geotiff geograhic data

header = strsplit(DATA(4).Header,{':',','});    % Header for the water chemistry matrix
header = header(2:end);                         % Eliminate the header label from the header
Data    = cat(1,DATA.wqData);                   % Water chemistry

% Get stats. on the data being processed
dataMax  = max(Data,[],1);
dataMean = mean(Data,1);
dataSTD  = std(Data,1);
dataMin  = min(Data,[],1);

windowName = 'Environmental Niche Modeling';
panalName = 'Parameters';

EnvWindow = GUIwindow(windowName);                              % Instanciate the GUI
EnvWindow.GUIpanels(panalName,'make','panel',[0.8 0 0.2 1]);    % Create putton panel for primary GUI functions

EnvWindow.Header.String = 'Generating Model Area';              % Prompt to select data
EnvWindow.Header.BackgroundColor = [0.4706    0.6706    0.6];   
EnvWindow.Axes.Position = [0.1 0.1 0.65 0.7];

Panel = EnvWindow.Panels(panalName);                            % Extract the button pane from its mapped container to access the structure
Panel.panel.BackgroundColor = [0.4706    0.6706    0.1882];

% Display data States. on a table the table
t = uitable(EnvWindow.Figure,'Data',[dataMax; dataMean; dataMin; dataSTD], 'Units','Normalized','Position',[0. 0.825 0.625 0.125]);
t.RowName = {'Data Max', 'Data Mean', 'Data Min', 'Data STD'};
t.ColumnName = header;

set(EnvWindow.Figure, 'pointer', 'watch')   % Set the pointer to a spinning wheel to indicate that the program is bussy
drawnow;

for ii = 1: length(header)      % Create parameter buttons on the main panel for DATA header
    
    EnvWindow = EnvWindow.GUIbuttons( panalName,'add',{'radio', header{ii}, [{@Parameters},   {EnvWindow},{panalName}], 0 });

end

% Create buttons for making the Env. Niche Model and ending the task
EnvWindow = EnvWindow.GUIbuttons( panalName,'add',...
    { 'space',      ' ',          [],                                         [];
      'pushbutton', 'Make Model', [{@EnvModel},     {EnvWindow},{panalName}], [];
      'pushbutton', 'Done',       [{@LoadEnvModel}, {EnvWindow},{panalName}], [];
    });

Panel = EnvWindow.Panels(panalName);   % Put the new version of the panel into its mapped container

% Create tool strips that explain the buttons' function
for ii = 1: length(header)
    Panel.button( ii).Radio.TooltipString = sprintf('Select parameter to add it to the environmental niche Model\nDeselesct parameter to remove it from model');
end

% Turn off "Make Model" and "Done" buttons
Panel.button(length(header)+2).Push.Visible  = 'off';  % Make Model
Panel.button(length(header)+3).Push.Visible  = 'off';  % Done

Panel.button( length(header)+2).Push.TooltipString = sprintf('Create environmental niche Model');
Panel.button( length(header)+3).Push.TooltipString = sprintf("I don't really do anything yet. \nBut I will!!");


% Deturmin properties to be used for the EKF filter when mapping water parameters
% Set outline for the EKF
if ~isempty(Boundaries) && isa(Boundaries, 'double')                                    % Use Boundaries if the Occ. Grid has been made
    outline = Boundaries;
    
elseif ~isempty(shape)                                                                  % Use the shape file in absence of the Occ. Grid

    if shape(1) > 0
        outline = fliplr(shape);                                                        % Make sure the waypoints are ordered [Lon Lat]
    else
        outline = shape;
    end
    
else                                                                                    % If both variables are empty, send an empty array
    outline = [];
end

vehicle   = cat(1,DATA.vehicle);                                                                    % Latitude, Longitude, Depth from surface

res = 1;                                                                                            % Gid cell resolution --> 1 meter
[xGrid, yGrid] = EKF_Prep(vehicle(:,2), vehicle(:,1), 'GridCell', res,'shape',outline);             % Process area information to prepare for water peramiter mapping
EnvWindow = EnvWindow.BackGround('LON',xGrid,'LAT',yGrid,'GeoData',geoData,'GeoImage',geoImage);    % Set the xGrid and yGrid ans georeferancing for the Envwindow.

if ~isempty(outline)
EnvWindow = EnvWindow.Addlayer('Boundaries', outline, panalName);                                   % Dispaly outline on the GUI
end

set(EnvWindow.Figure, 'pointer', 'arrow')                                                           % Stop the pointer spinning wheel
EnvWindow.Header.String = 'Select Data Parameters For Env. Modle';                                  % Prompt to select data
drawnow;
end


function Parameters(varargin)
% Varargin: {'UIControl', 'matlab.ui.eventdata.ActionData', 'GUIwindow', 'panalName'}
global EnvWindow;       % GUI window
global Model;           % Area model being developed by this app
global DATA;            % Sensor data

EnvWindow.Header.String = 'Select / Deselect Data Parameters For Env. Modle or Set Thresholds'; % Prompt user to choose parameters

% Creat a button Panel for working with the Env. Niche
panelName = 'Niche Model';
allPanels = keys(EnvWindow.Panels);
if ~contains(allPanels, panelName)

    EnvWindow.Axes.Position = [0.1 0.1 0.55 0.7];                                   % Resize the GUI's dispaly area to accomidate for the new panel1
    EnvWindow = EnvWindow.GUIpanels(panelName,'make','panel',[0.7 0 0.1 1]);        % Create Panel at position [0.7 0 0.1 1]
    Panel = EnvWindow.Panels(panelName);                                            % Extract the button pane from its mapped container to access the structure
    Panel.panel.BackgroundColor = [0.9294    0.6902    0.1294];
    EnvWindow = EnvWindow.GUIbuttons( panelName,'add',{'text', 'Thresholds', [], [] });
end

parameters = cat(1,Model.MapParameter);
index = ismember(parameters, varargin{1}.Tag);

% If Layer was just turned on generate a parameter map _________________________________________________________________________________
if varargin{2}.Source.Value && ~any(index)
    % Isolate the selected parameter
    header = strsplit(DATA(4).Header,{':',','});            % Header for the water chemistry matrix
    header = header(2:end);                                 % Eliminate the header label from the header
    column = ismember(header, varargin{2}.Source.String);   % Get column index of the selected peramiter
    
    % Position data --> Latitude, Longitude, Depth from surface
    vehicle   = cat(1,DATA.vehicle);                        % Extract the data from the DATA structure                   
    latitude  = vehicle(:,1);
    longitude = vehicle(:,2);
    
    % Peramiter data
    data      = cat(1,DATA.wqData);                         % Extract the data from the DATA structure  
    parameter = data(:,column);                             % Isolate the parameter of intrest
    
    % filter settings
    window = 40;    % Size of the filter windo
    sig = 2;        % Number of standarad deviation to filter out --> filter out outliers
    
    set(EnvWindow.Figure, 'pointer', 'watch')   % Set mouse pointer to spinning wheel to indicate bussy
    drawnow;
    
    parameterMap = EKF( longitude, latitude, parameter,'Window', window, 'sigma', sig );    % Make the water parameter map
    
    set(EnvWindow.Figure, 'pointer', 'arrow')
    drawnow;
    
    EnvWindow = EnvWindow.Addlayer(varargin{2}.Source.String,  parameterMap, varargin{4});   % Dispaly map on the GUI
    
    EnvWindow = EnvWindow.GUIbuttons( panelName,'add',{'edit', varargin{2}.Source.String, [{@SetThresholds},   {EnvWindow},{panelName}], 0 });  % add thrshold entrie buttons to the GUI
    
    % Add water parameter map to the area Model
    l = length(Model)+1;
    Model(l).WaterMap = parameterMap;                                           % Add parameter map
    Model(l).MapParameter = {varargin{2}.Source.String};                        % Indicate which peramiter has been mapped ie: temperature
    Model(l).PeramiterSettings = [max(parameterMap(:)), min(parameterMap(:))];  % Threshold values are initaly set to the max and min values of the parameter map
    Model(l).WaterMapIO = true;                                                 % Set parameter IO to true so that the parameter will be included in the env. niche model
    
    
% Parameter was turned back on. Map already exists, re-activate it _____________________________________________________________________
elseif varargin{2}.Source.Value && any(index)
    Model(index).WaterMapIO = varargin{2}.Source.Value;     % Set IO value to true so that it will be included in the Env. Niche Model
    EnvWindow = EnvWindow.RestoreLayer(varargin{1}.Tag);    % Show Layer on the GUI display
    EnvWindow = EnvWindow.GUIbuttons( panelName,'add',{'edit', varargin{2}.Source.String, [{@SetThresholds},   {EnvWindow},{panelName}], 0 });  % Re-generate the buttons
    
    
    
% If parameter has been de-selected, turn off the layer and remove buttons _____________________________________________________________
elseif any(index)
    Model(index).WaterMapIO = varargin{2}.Source.Value;                         % Set the IO value to false so that it does not get included in the Evn. Niche Model
    EnvWindow = EnvWindow.GUIbuttons( panelName, 'remove', {varargin{1}.Tag});  % Remove buttons
    EnvWindow = EnvWindow.HideLayer( varargin{1}.Tag );                         % Remove area from GUI display
    
else
    warning('Unrecognized state')
end

end


function SetThresholds(varargin)
global EnvWindow;       % Environmental Niche Modeling GUI window
global Model; 

parameters = cat(1,Model.MapParameter);             % Get the index of the specified parameter 
index = ismember(parameters, varargin{1}.Tag);

a = Model(index).PeramiterSettings;                 % Get the existing values of the parameters 

% Modify the theshold that was set in the GUI
if varargin{1}.Value
    a(1) = str2double(varargin{2}.Source.String);   % Set Max value
else
    a(2) = str2double(varargin{2}.Source.String);   % Set Min value
end

Model(index).PeramiterSettings = a;  % Reassign the values to the model


EnvWindow.Header.String = 'Select / Deselect Data Parameters For Env. Modle, Set Thresholds or Make Env. Model'; % Prompt to select data

% Make the "Make Model" button visible
Panel = EnvWindow.Panels('Parameters');   % Get Panel from its mapped container
Panel.button(length(Panel.button)-1).Push.Visible  = 'on';  % Make Model

end


% Environmental Niche Modeling Happens Here !!!!!!!!!!!!!!!
function EnvModel(varargin)
global EnvWindow;       % GUI window
global Model;           % Area model being developed by this app
global ekfProperties    % Base values used for the EKF ie. area grid, origen, grid size
global xGrid;
global yGrid; 

disp('Make Env. Model')

EnvModel = double(ekfProperties{3});    % Get area grid

% Cycle through the water parameter maps
for iter = 1:length(Model)
    
    if Model(iter).WaterMapIO               % If selected, include the parameter in the model 
        disp(Model(iter).MapParameter)      % Display the parameters being included in the Model
        
        
        range = Model(iter).PeramiterSettings;  % Get the parameter thresholds
        range(1);                               % Upper threshold
        range(2);                               % Lower threshold
        
        % Do Math Here -------------------------------------------------------------------------------------
        EnvModel = ????

        
        % --------------------------------------------------------------------------------------------------
        
    else
        continue
    end
end

% Display the Environmental Niche Model on the GUI
EnvWindow = EnvWindow.Addlayer('Env. Nich Model',  EnvModel, varargin{4});  

end


function LoadEnvModel(varargin)
panelName = 'Niche Model';

varargin{3}.GUIpanels(panelName,'delete');  % Close the button panel for the Environmental Niche Model
end





%% Supporitng Functions ==================================================================

function [xGrid, yGrid] = EKF_Prep(x, y, varargin)
global ekfProperties;   % Values used to set up the EKF filter

% Initalize parameters that can be change by the varargin --------------------------------------------------------------
gridCellResolution = 0.1;
shape = [];

% Get Additional Input peramiters ---------------------------------------------------------------------------------------
if  nargin > 1
    
    for i=1:length(varargin)
        try
            
            switch lower(cell2mat(varargin(i)))
                
                case 'gridcell'                             % Get grid cell resolution
                    gridCellResolution = varargin{i+1};
                    
                    
                case 'shape'                                % Get outline for the filter area
                    shape = varargin{i+1};
                    
                otherwise
                    continue
            end
            
        catch
            continue
            
        end
    end
end

% Create boundary map ------------------------------------------------------------------------------------

if isempty(shape) 
    k = boundary(x,y);
    edgePoints = [x(k),y(k)];
else
    edgePoints = shape;
    if edgePoints(1,1) > 0
        edgePoints = fliplr(edgePoints);
    end
end

% Finde the origen of the data set
delta = 0.00008;
Origin(1) = min(edgePoints(:,1))-delta;
Origin(2) = min(edgePoints(:,2))-delta;

% Deteruming the oposite corner of the data grid
Lmax(1) = max(edgePoints(:,1))+delta;
Lmax(2) = max(edgePoints(:,2))+delta;

% Get dimensions in meters
%  s = vdist(lat1, lon1, lat2,   lon2)
xDim = vdist(Origin(2),Origin(1),Origin(2),  Lmax(1));  % Change in Longitude
yDim = vdist(Origin(2),Origin(1),Lmax(2),Origin(1));    % Change in Latatude

% Size of the map  m-rows x n-columns
m = ceil(yDim/gridCellResolution);
n = ceil(xDim/gridCellResolution);

% Create Area mesh
x_vec = linspace(Origin(1),Lmax(1), n);
y_vec = linspace(Origin(2),Lmax(2), m);
[xGrid, yGrid] = meshgrid(x_vec, y_vec);

% Create a logic map of the area insied the bound area
dataArea    = inpolygon(xGrid,yGrid,edgePoints(:,1),edgePoints(:,2));


ekfProperties{1} = Origin;
ekfProperties{2} = gridCellResolution;
ekfProperties{3} = dataArea;

end


